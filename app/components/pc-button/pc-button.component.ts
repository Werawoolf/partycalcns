import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  moduleId: module.id,
  selector: "pc-button",
  templateUrl: "pc-button.component.html",
  styleUrls: ['pc-button.component.style.css']
})
export class PCButtonComponent { 
    @Input() label: string;
    @Output() onTap = new EventEmitter(); 

    tap() {
      this.onTap.emit();
    }
}