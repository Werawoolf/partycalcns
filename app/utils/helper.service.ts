import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {
    shortId: Function = shortId
}

export function shortId(): string {
    return Math.random().toString(36).slice(-6);
}