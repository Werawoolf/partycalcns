import { Entity } from './entity';

export class User extends Entity {
    public name: string;

    constructor(name: string) {
        super();

        this.name = name;
    }
}