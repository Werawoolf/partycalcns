import { shortId } from '../utils/helper.service';

export class Entity {
    public id: string;

    constructor() {
        this.id = shortId();    
    }
}