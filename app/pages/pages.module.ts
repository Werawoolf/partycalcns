import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModule } from "@angular/core";

import { PageRouting } from './pages.routing';
import { ComponentsModule } from '../components/components.module';
import { ContributorsModule } from './contributors/contributors.module';

import { HomeComponent } from "./home/home.component";

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    ComponentsModule,
    ContributorsModule,

    PageRouting
  ],
  declarations: [HomeComponent]
})
export class PagesModule { }
