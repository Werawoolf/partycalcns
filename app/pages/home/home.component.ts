import { Component, OnInit } from "@angular/core";
import { Router } from '@angular/router';
import * as consts from "../../utils/const.service";
import { SecureStorage } from "nativescript-secure-storage";
import { User } from '../../models';

@Component({
  moduleId: module.id,
  selector: "home",
  templateUrl: "home.component.html",
  styleUrls: ['home.component.style.css']
})
export class HomeComponent implements OnInit {
  currentUser: User;
  shownSavedData: boolean;
  userName: string;
  storage: SecureStorage;

  constructor(private router: Router) {
    this.storage = new SecureStorage();
  }

  public ngOnInit(): void {
    this.storage
      .get({ key: consts.CURRENT_USER_TOKEN_LS })
      .then(u => {
        this.setCurrentUser(JSON.parse(u));
      });
  }

  public continue(): void {
    let user = this.currentUser;
    if (user && user.name)
      return this.goToNext();

    if (this.userName) {
      this.currentUser = new User(this.userName.trim());
      this.storage
        .set({
          key: consts.CURRENT_USER_TOKEN_LS,
          value: JSON.stringify(this.currentUser)
        })
        .then(res => {
          if (res) {
            this.userName = "";
            this.setCurrentUser(this.currentUser);

            return this.goToNext();
          };
        });
    }

    return this.goToNext();
  }

  public setCurrentUser(u: User): void {
    this.currentUser = u;
    this.shownSavedData = !!u && !!u.name;
  }

  private goToNext(): void {
    this.router.navigate(['/contributors']);
  }
}
