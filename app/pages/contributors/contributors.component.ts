import { Component } from "@angular/core";
import { Page } from "ui/page";

@Component({
    moduleId: module.id,
    selector: "contributors",
    templateUrl: "contributors.component.html",
    styleUrls: ['contributors.component.style.css']
})
export class ContributorsComponent {
    constructor(private page: Page) { }
}