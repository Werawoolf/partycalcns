"use strict";
var platform_1 = require("nativescript-angular/platform");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var pages_routing_1 = require('./pages.routing');
var components_module_1 = require('../components/components.module');
var home_component_1 = require("./home/home.component");
var PagesModule = (function () {
    function PagesModule() {
    }
    PagesModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule,
                components_module_1.ComponentsModule,
                pages_routing_1.PageRouting
            ],
            declarations: [home_component_1.HomeComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], PagesModule);
    return PagesModule;
}());
exports.PagesModule = PagesModule;
//# sourceMappingURL=pages.module.js.map