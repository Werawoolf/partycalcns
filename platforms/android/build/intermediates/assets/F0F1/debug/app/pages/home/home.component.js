"use strict";
var core_1 = require("@angular/core");
var helper_service_1 = require("../../utils/helper.service");
var consts = require("../../utils/const.service");
var nativescript_secure_storage_1 = require("nativescript-secure-storage");
var models_1 = require('../../models');
var HomeComponent = (function () {
    function HomeComponent(helper) {
        this.helper = helper;
        this.storage = new nativescript_secure_storage_1.SecureStorage();
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.storage
            .get({ key: consts.CURRENT_USER_TOKEN_LS })
            .then(function (u) {
            if (typeof u === undefined)
                u = null;
            u = JSON.parse(u);
            _this.currentUser = u;
            _this.shownSavedData = u && u.name;
        });
    };
    HomeComponent.prototype.continue = function () {
        if (!this.userName)
            return false;
        this.currentUser = new models_1.User(this.userName);
        this.storage
            .set({
            key: consts.CURRENT_USER_TOKEN_LS,
            value: JSON.stringify(this.currentUser)
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "home",
            templateUrl: "home.component.html",
            styleUrls: ['home.component.style.css'],
            providers: [helper_service_1.HelperService]
        }), 
        __metadata('design:paramtypes', [helper_service_1.HelperService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map