import { Component, OnInit } from "@angular/core";
import { HelperService } from "../../utils/helper.service";
import * as consts from "../../utils/const.service";
import { SecureStorage } from "nativescript-secure-storage";
import { User } from '../../models';

@Component({
  moduleId: module.id,
  selector: "home",
  templateUrl: "home.component.html",
  styleUrls: ['home.component.style.css'],
  providers: [HelperService]
})
export class HomeComponent implements OnInit {
  currentUser: User;
  shownSavedData: boolean;
  userName: string;
  storage: SecureStorage;

  constructor(private helper: HelperService) {
    this.storage = new SecureStorage();
  }

  ngOnInit() {
    this.storage
      .get({ key: consts.CURRENT_USER_TOKEN_LS })
      .then(u => {
        if (typeof u === undefined)
          u = null;

        u = JSON.parse(u);

        this.currentUser = u;
        this.shownSavedData = u && u.name;
      });
  }

  continue() {
    if (!this.userName) return false;

    this.currentUser = new User(this.userName);
    this.storage
      .set({
        key: consts.CURRENT_USER_TOKEN_LS,
        value: JSON.stringify(this.currentUser)
      });
  }
}
