"use strict";
var entity_1 = require('./entity');
var User = (function (_super) {
    __extends(User, _super);
    function User(name) {
        _super.call(this);
        this.name = name;
    }
    return User;
}(entity_1.Entity));
exports.User = User;
//# sourceMappingURL=user.js.map