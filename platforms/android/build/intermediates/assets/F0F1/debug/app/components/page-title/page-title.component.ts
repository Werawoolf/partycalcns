import { Component, Input } from "@angular/core";

@Component({
  moduleId: module.id,
  selector: "page-title",
  templateUrl: "page-title.component.html",
  styleUrls: ['page-title.component.style.css']
})
export class PageTitleComponent { 
    @Input() caption: string;
}