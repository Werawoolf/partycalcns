"use strict";
var router_1 = require("@angular/router");
var home_component_1 = require("./home/home.component");
var PageRoutes = [
    { path: "home", component: home_component_1.HomeComponent },
];
exports.PageRouting = router_1.RouterModule.forChild(PageRoutes);
//# sourceMappingURL=pages.routing.js.map