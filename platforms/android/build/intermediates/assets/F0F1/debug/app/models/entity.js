"use strict";
var helper_service_1 = require('../utils/helper.service');
var Entity = (function () {
    function Entity() {
        this.id = helper_service_1.shortId();
    }
    return Entity;
}());
exports.Entity = Entity;
//# sourceMappingURL=entity.js.map