import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";

const PageRoutes: Routes = [
    { path: "home", component: HomeComponent },
];
export const PageRouting: ModuleWithProviders = RouterModule.forChild(PageRoutes);