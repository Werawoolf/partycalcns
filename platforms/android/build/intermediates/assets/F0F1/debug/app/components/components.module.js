"use strict";
var platform_1 = require("nativescript-angular/platform");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var page_title_component_1 = require('./page-title/page-title.component');
var pc_button_component_1 = require('./pc-button/pc-button.component');
var Components = [page_title_component_1.PageTitleComponent, pc_button_component_1.PCButtonComponent];
var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule
            ],
            exports: Components,
            declarations: Components
        }), 
        __metadata('design:paramtypes', [])
    ], ComponentsModule);
    return ComponentsModule;
}());
exports.ComponentsModule = ComponentsModule;
//# sourceMappingURL=components.module.js.map