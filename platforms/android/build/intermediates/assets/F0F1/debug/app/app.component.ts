import { Component } from "@angular/core";
import { Page } from "ui/page";

@Component({
  moduleId: module.id,
  selector: "party-calc",
  template: "<page-router-outlet></page-router-outlet>",
  styleUrls: ['app.css']
})
export class AppComponent {
  constructor(page: Page) {
    page.actionBarHidden = true;
  }
}