"use strict";
var core_1 = require('@angular/core');
var HelperService = (function () {
    function HelperService() {
        this.shortId = shortId;
    }
    HelperService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], HelperService);
    return HelperService;
}());
exports.HelperService = HelperService;
function shortId() {
    return Math.random().toString(36).slice(-6);
}
exports.shortId = shortId;
//# sourceMappingURL=helper.service.js.map