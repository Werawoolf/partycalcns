"use strict";
var core_1 = require("@angular/core");
var PCButtonComponent = (function () {
    function PCButtonComponent() {
        this.onTap = new core_1.EventEmitter();
    }
    PCButtonComponent.prototype.tap = function () {
        this.onTap.emit();
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PCButtonComponent.prototype, "label", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], PCButtonComponent.prototype, "onTap", void 0);
    PCButtonComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "pc-button",
            templateUrl: "pc-button.component.html",
            styleUrls: ['pc-button.component.style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], PCButtonComponent);
    return PCButtonComponent;
}());
exports.PCButtonComponent = PCButtonComponent;
//# sourceMappingURL=pc-button.component.js.map