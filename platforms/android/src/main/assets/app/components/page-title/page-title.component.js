"use strict";
var core_1 = require("@angular/core");
var PageTitleComponent = (function () {
    function PageTitleComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PageTitleComponent.prototype, "caption", void 0);
    PageTitleComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "page-title",
            templateUrl: "page-title.component.html",
            styleUrls: ['page-title.component.style.css']
        }), 
        __metadata('design:paramtypes', [])
    ], PageTitleComponent);
    return PageTitleComponent;
}());
exports.PageTitleComponent = PageTitleComponent;
//# sourceMappingURL=page-title.component.js.map