import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModule } from "@angular/core";

import { PCButtonComponent } from './pc-button/pc-button.component';

const Components = [PCButtonComponent];

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule
  ],
  exports: Components,
  declarations: Components
})
export class ComponentsModule { }