"use strict";
var router_1 = require("@angular/router");
var home_component_1 = require("./home/home.component");
var contributors_component_1 = require('./contributors/contributors.component');
var PageRoutes = [
    { path: "home", component: home_component_1.HomeComponent },
    { path: "contributors", component: contributors_component_1.ContributorsComponent },
];
exports.PageRouting = router_1.RouterModule.forChild(PageRoutes);
//# sourceMappingURL=pages.routing.js.map