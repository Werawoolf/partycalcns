import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./home/home.component";
import { ContributorsComponent } from './contributors/contributors.component';

const PageRoutes: Routes = [
    { path: "home", component: HomeComponent },
    { path: "contributors", component: ContributorsComponent },
];
export const PageRouting: ModuleWithProviders = RouterModule.forChild(PageRoutes);