"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var ContributorsComponent = (function () {
    function ContributorsComponent(page) {
        this.page = page;
    }
    ContributorsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "contributors",
            templateUrl: "contributors.component.html",
            styleUrls: ['contributors.component.style.css']
        }), 
        __metadata('design:paramtypes', [page_1.Page])
    ], ContributorsComponent);
    return ContributorsComponent;
}());
exports.ContributorsComponent = ContributorsComponent;
//# sourceMappingURL=contributors.component.js.map