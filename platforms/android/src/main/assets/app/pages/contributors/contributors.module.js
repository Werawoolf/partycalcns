"use strict";
var platform_1 = require("nativescript-angular/platform");
var forms_1 = require("nativescript-angular/forms");
var core_1 = require("@angular/core");
var components_module_1 = require('../../components/components.module');
var contributors_component_1 = require("./contributors.component");
var ContributorsModule = (function () {
    function ContributorsModule() {
    }
    ContributorsModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_1.NativeScriptModule,
                forms_1.NativeScriptFormsModule,
                components_module_1.ComponentsModule,
            ],
            exports: [contributors_component_1.ContributorsComponent],
            declarations: [contributors_component_1.ContributorsComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], ContributorsModule);
    return ContributorsModule;
}());
exports.ContributorsModule = ContributorsModule;
//# sourceMappingURL=contributors.module.js.map