import { NativeScriptModule } from "nativescript-angular/platform";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NgModule } from "@angular/core";

import { ComponentsModule } from '../../components/components.module';

import { ContributorsComponent } from "./contributors.component";

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        ComponentsModule,
    ],
    exports: [ContributorsComponent],
    declarations: [ContributorsComponent]
})
export class ContributorsModule { }
