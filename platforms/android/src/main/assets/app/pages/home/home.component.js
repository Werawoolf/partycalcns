"use strict";
var core_1 = require("@angular/core");
var router_1 = require('@angular/router');
var consts = require("../../utils/const.service");
var nativescript_secure_storage_1 = require("nativescript-secure-storage");
var models_1 = require('../../models');
var HomeComponent = (function () {
    function HomeComponent(router) {
        this.router = router;
        this.storage = new nativescript_secure_storage_1.SecureStorage();
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.storage
            .get({ key: consts.CURRENT_USER_TOKEN_LS })
            .then(function (u) {
            _this.setCurrentUser(JSON.parse(u));
        });
    };
    HomeComponent.prototype.continue = function () {
        var _this = this;
        var user = this.currentUser;
        if (user && user.name)
            return this.goToNext();
        if (this.userName) {
            this.currentUser = new models_1.User(this.userName.trim());
            this.storage
                .set({
                key: consts.CURRENT_USER_TOKEN_LS,
                value: JSON.stringify(this.currentUser)
            })
                .then(function (res) {
                if (res) {
                    _this.userName = "";
                    _this.setCurrentUser(_this.currentUser);
                    return _this.goToNext();
                }
                ;
            });
        }
        return this.goToNext();
    };
    HomeComponent.prototype.setCurrentUser = function (u) {
        this.currentUser = u;
        this.shownSavedData = !!u && !!u.name;
    };
    HomeComponent.prototype.goToNext = function () {
        this.router.navigate(['/contributors']);
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "home",
            templateUrl: "home.component.html",
            styleUrls: ['home.component.style.css']
        }), 
        __metadata('design:paramtypes', [router_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map