"use strict";
var core_1 = require("@angular/core");
var page_1 = require("ui/page");
var AppComponent = (function () {
    function AppComponent(page) {
    }
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "party-calc",
            template: "<page-router-outlet></page-router-outlet>",
            styleUrls: ['app.css']
        }), 
        __metadata('design:paramtypes', [page_1.Page])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map